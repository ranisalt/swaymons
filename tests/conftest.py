import pytest
from swaymons import create_parser, Monitor


@pytest.fixture
def parser():
    return create_parser()


@pytest.fixture
def outputs():
    return [
        Monitor(name='LVDS-1', rect={
            "x": 1920,
            "y": 0,
            "width": 1366,
            "height": 768,
        }, active=True, modes=[
            {
                "width": 1366,
                "height": 768,
                "refresh": 60000,
            },
        ], current_mode={
            "width": 1366,
            "height": 768,
            "refresh": 60000,
        }),
        Monitor(name='VGA-1', rect={
            "x": 0,
            "y": 0,
            "width": 1920,
            "height": 1080,
        }, active=True, modes=[
            {
                "width": 1280,
                "height": 720,
                "refresh": 75000,
            },
            {
                "width": 1920,
                "height": 1080,
                "refresh": 60000,
            },
        ], current_mode={
            "width": 1920,
            "height": 1080,
            "refresh": 60000,
        }),
    ]
