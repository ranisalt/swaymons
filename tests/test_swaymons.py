import pytest
from swaymons import prepare_commands


@pytest.mark.test
def test_parser_extend(parser):
    args = parser.parse_args(['-p', 'LVDS-1', '-e', 'left'])
    assert not args.o
    assert not args.s
    assert args.extend == 'left'
    assert args.primary == 'LVDS-1'


@pytest.mark.test
def test_parser_primary_only(parser):
    args = parser.parse_args(['-o'])
    assert args.o
    assert not args.s
    assert not args.extend
    assert not args.primary


@pytest.mark.test
def test_parser_secondary_only(parser):
    args = parser.parse_args(['-s'])
    assert not args.o
    assert args.s
    assert not args.extend
    assert not args.primary


@pytest.mark.test
def test_primary_only(parser, outputs):
    args = parser.parse_args(['-p', 'LVDS-1', '-o'])
    commands = prepare_commands(args, outputs)
    assert len(commands) == len(outputs)
    assert 'output LVDS-1 res 1366x768 pos 0 0' in commands
    assert 'output VGA-1 disable' in commands


@pytest.mark.test
def test_secondary_only(parser, outputs):
    args = parser.parse_args(['-p', 'LVDS-1', '-s'])
    commands = prepare_commands(args, outputs)
    assert len(commands) == len(outputs)
    assert 'output LVDS-1 disable' in commands
    assert 'output VGA-1 res 1920x1080 pos 0 0' in commands


@pytest.mark.test
def test_extend(parser, outputs):
    args = parser.parse_args(['-p', 'LVDS-1', '-e', 'top'])
    commands = prepare_commands(args, outputs)
    assert len(commands) == len(outputs)
    assert 'output LVDS-1 res 1366x768 pos 0 1080' in commands
    assert 'output VGA-1 res 1920x1080 pos 0 0' in commands

    args = parser.parse_args(['-p', 'LVDS-1', '-e', 'left'])
    commands = prepare_commands(args, outputs)
    assert len(commands) == len(outputs)
    assert 'output LVDS-1 res 1366x768 pos 1920 0' in commands
    assert 'output VGA-1 res 1920x1080 pos 0 0' in commands

    args = parser.parse_args(['-p', 'LVDS-1', '-e', 'right'])
    commands = prepare_commands(args, outputs)
    assert len(commands) == len(outputs)
    assert 'output LVDS-1 res 1366x768 pos 0 0' in commands
    assert 'output VGA-1 res 1920x1080 pos 1366 0' in commands

    args = parser.parse_args(['-p', 'LVDS-1', '-e', 'bottom'])
    commands = prepare_commands(args, outputs)
    assert len(commands) == len(outputs)
    assert 'output LVDS-1 res 1366x768 pos 0 0' in commands
    assert 'output VGA-1 res 1920x1080 pos 0 768' in commands


@pytest.mark.test
def test_query(capsys, parser, outputs):
    args = parser.parse_args([])
    assert prepare_commands(args, outputs) == []

    out, _ = capsys.readouterr()
    lines = out.split('\n')
    assert 'Monitors: 2' in lines
    assert 'Mode: extend' in lines
    assert '0: LVDS-1 (enabled)' in lines
    assert '1: VGA-1 (enabled)' in lines


@pytest.mark.test
def test_query_set_primary(capsys, parser, outputs):
    args = parser.parse_args(['-p', 'LVDS-1'])
    assert prepare_commands(args, outputs) == []

    out, _ = capsys.readouterr()
    lines = out.split('\n')
    assert '0: LVDS-1* (enabled)' in lines


@pytest.mark.test
def test_query_primary(capsys, parser, outputs):
    outputs[1].active = False
    args = parser.parse_args(['-p', 'LVDS-1'])
    assert prepare_commands(args, outputs) == []

    out, _ = capsys.readouterr()
    lines = out.split('\n')
    assert 'Mode: primary' in lines
    assert '1: VGA-1 (enabled)' not in lines


@pytest.mark.test
def test_query_secondary(capsys, parser, outputs):
    outputs[0].active = False
    args = parser.parse_args(['-p', 'LVDS-1'])
    assert prepare_commands(args, outputs) == []

    out, _ = capsys.readouterr()
    lines = out.split('\n')
    assert 'Mode: secondary' in lines
    assert '0: LVDS-1* (enabled)' not in lines
